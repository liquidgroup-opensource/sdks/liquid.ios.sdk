/*
 * This file is part of the SDLiqWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

#import <Foundation/Foundation.h>
#import "SDLiqWebImageCompat.h"
#import "SDLiqWebImageDefine.h"

@class SDLiqWebImageOptionsResult;

typedef SDLiqWebImageOptionsResult * _Nullable(^SDLiqWebImageOptionsProcessorBlock)(NSURL * _Nullable url, SDLiqWebImageOptions options, SDLiqWebImageContext * _Nullable context);

/**
 The options result contains both options and context.
 */
@interface SDLiqWebImageOptionsResult : NSObject

/**
 WebCache options.
 */
@property (nonatomic, assign, readonly) SDLiqWebImageOptions options;

/**
 Context options.
 */
@property (nonatomic, copy, readonly, nullable) SDLiqWebImageContext *context;

/**
 Create a new options result.

 @param options options
 @param context context
 @return The options result contains both options and context.
 */
- (nonnull instancetype)initWithOptions:(SDLiqWebImageOptions)options context:(nullable SDLiqWebImageContext *)context;

@end

/**
 This is the protocol for options processor.
 Options processor can be used, to control the final result for individual image request's `SDLiqWebImageOptions` and `SDLiqWebImageContext`
 Implements the protocol to have a global control for each indivadual image request's option.
 */
@protocol SDLiqWebImageOptionsProcessor <NSObject>

/**
 Return the processed options result for specify image URL, with its options and context

 @param url The URL to the image
 @param options A mask to specify options to use for this request
 @param context A context contains different options to perform specify changes or processes, see `SDLiqWebImageContextOption`. This hold the extra objects which `options` enum can not hold.
 @return The processed result, contains both options and context
 */
- (nullable SDLiqWebImageOptionsResult *)processedResultForURL:(nullable NSURL *)url
                                                    options:(SDLiqWebImageOptions)options
                                                    context:(nullable SDLiqWebImageContext *)context;

@end

/**
 A options processor class with block.
 */
@interface SDLiqWebImageOptionsProcessor : NSObject<SDLiqWebImageOptionsProcessor>

- (nonnull instancetype)initWithBlock:(nonnull SDLiqWebImageOptionsProcessorBlock)block;
+ (nonnull instancetype)optionsProcessorWithBlock:(nonnull SDLiqWebImageOptionsProcessorBlock)block;

@end
