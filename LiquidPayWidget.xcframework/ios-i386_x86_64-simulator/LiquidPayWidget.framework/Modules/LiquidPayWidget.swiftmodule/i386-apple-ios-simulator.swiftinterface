// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.6 (swiftlang-5.6.0.323.62 clang-1316.0.20.8)
// swift-module-flags: -target i386-apple-ios10.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name LiquidPayWidget
import ACPAnalytics
import ACPCore
import LiquidPayWidget.Private.APNGImageSerialization
import AVFoundation
import LiquidPayWidget.Private.CarbonSwipeRefresh
import LiquidPayWidget.Private.CardIO
import CommonCrypto
import CoreLocation
import CoreTelephony
import Foundation
import LiquidPayWidget.Private.LP_BKCardNumberFormatter
import LiquidPayWidget.Private.LP_MBProgressHUD
@_exported import LiquidPayWidget
import LocalAuthentication
import LiquidPayWidget.Private.Luhn
import MapKit
import MessageUI
import SafariServices
import Swift
import LiquidPayWidget.Private.TGLStackedViewController
import LiquidPayWidget.Private.UIImageView_WebCache
import UIKit
import WebKit
import LiquidPayWidget.Private.XLPagerTabStrip
import _Concurrency
public enum PagerTabStripBehaviour {
  case common(skipIntermediateViewControllers: Swift.Bool)
  case progressive(skipIntermediateViewControllers: Swift.Bool, elasticIndicatorLimit: Swift.Bool)
  public var skipIntermediateViewControllers: Swift.Bool {
    get
  }
  public var isProgressiveIndicator: Swift.Bool {
    get
  }
  public var isElasticIndicatorLimit: Swift.Bool {
    get
  }
}
public protocol IndicatorInfoProvider {
  func indicatorInfo(for pagerTabStripController: LiquidPayWidget.PagerTabStripViewController) -> LiquidPayWidget.IndicatorInfo
}
public protocol PagerTabStripDelegate : AnyObject {
  func updateIndicator(for viewController: LiquidPayWidget.PagerTabStripViewController, fromIndex: Swift.Int, toIndex: Swift.Int)
}
public protocol PagerTabStripIsProgressiveDelegate : LiquidPayWidget.PagerTabStripDelegate {
  func updateIndicator(for viewController: LiquidPayWidget.PagerTabStripViewController, fromIndex: Swift.Int, toIndex: Swift.Int, withProgressPercentage progressPercentage: CoreGraphics.CGFloat, indexWasChanged: Swift.Bool)
}
public protocol PagerTabStripDataSource : AnyObject {
  func viewControllers(for pagerTabStripController: LiquidPayWidget.PagerTabStripViewController) -> [UIKit.UIViewController]
}
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) open class PagerTabStripViewController : UIKit.UIViewController, UIKit.UIScrollViewDelegate {
  @objc @IBOutlet @_Concurrency.MainActor(unsafe) weak public var containerView: UIKit.UIScrollView!
  @_Concurrency.MainActor(unsafe) weak open var delegate: LiquidPayWidget.PagerTabStripDelegate?
  @_Concurrency.MainActor(unsafe) weak open var datasource: LiquidPayWidget.PagerTabStripDataSource?
  @_Concurrency.MainActor(unsafe) open var pagerBehaviour: LiquidPayWidget.PagerTabStripBehaviour
  @_Concurrency.MainActor(unsafe) open var viewControllers: [UIKit.UIViewController] {
    get
  }
  @_Concurrency.MainActor(unsafe) open var currentIndex: Swift.Int
  @_Concurrency.MainActor(unsafe) open var preCurrentIndex: Swift.Int {
    get
  }
  @_Concurrency.MainActor(unsafe) open var pageWidth: CoreGraphics.CGFloat {
    get
  }
  @_Concurrency.MainActor(unsafe) open var scrollPercentage: CoreGraphics.CGFloat {
    get
  }
  @_Concurrency.MainActor(unsafe) open var swipeDirection: LiquidPayWidget.SwipeDirection {
    get
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewDidLoad()
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewWillAppear(_ animated: Swift.Bool)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewDidAppear(_ animated: Swift.Bool)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewWillDisappear(_ animated: Swift.Bool)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewDidDisappear(_ animated: Swift.Bool)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewDidLayoutSubviews()
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var shouldAutomaticallyForwardAppearanceMethods: Swift.Bool {
    @_Concurrency.MainActor(unsafe) @objc get
  }
  @_Concurrency.MainActor(unsafe) open func moveToViewController(at index: Swift.Int, animated: Swift.Bool = true)
  @_Concurrency.MainActor(unsafe) open func moveTo(viewController: UIKit.UIViewController, animated: Swift.Bool = true)
  @_Concurrency.MainActor(unsafe) open func viewControllers(for pagerTabStripController: LiquidPayWidget.PagerTabStripViewController) -> [UIKit.UIViewController]
  @_Concurrency.MainActor(unsafe) open func updateIfNeeded()
  @_Concurrency.MainActor(unsafe) open func canMoveTo(index: Swift.Int) -> Swift.Bool
  @_Concurrency.MainActor(unsafe) open func pageOffsetForChild(at index: Swift.Int) -> CoreGraphics.CGFloat
  @_Concurrency.MainActor(unsafe) open func offsetForChild(at index: Swift.Int) -> CoreGraphics.CGFloat
  @_Concurrency.MainActor(unsafe) open func offsetForChild(viewController: UIKit.UIViewController) throws -> CoreGraphics.CGFloat
  @_Concurrency.MainActor(unsafe) open func pageFor(contentOffset: CoreGraphics.CGFloat) -> Swift.Int
  @_Concurrency.MainActor(unsafe) open func virtualPageFor(contentOffset: CoreGraphics.CGFloat) -> Swift.Int
  @_Concurrency.MainActor(unsafe) open func pageFor(virtualPage: Swift.Int) -> Swift.Int
  @_Concurrency.MainActor(unsafe) open func updateContent()
  @_Concurrency.MainActor(unsafe) open func reloadPagerTabStripView()
  @_Concurrency.MainActor(unsafe) @objc open func scrollViewDidScroll(_ scrollView: UIKit.UIScrollView)
  @_Concurrency.MainActor(unsafe) @objc open func scrollViewWillBeginDragging(_ scrollView: UIKit.UIScrollView)
  @_Concurrency.MainActor(unsafe) @objc open func scrollViewDidEndScrollingAnimation(_ scrollView: UIKit.UIScrollView)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewWillTransition(to size: CoreGraphics.CGSize, with coordinator: UIKit.UIViewControllerTransitionCoordinator)
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
@objc public enum LPWidgetStatus : Swift.Int {
  case ProfileUpdateRequiredError = 100, RewardPointsRedirectionError = 200, LuckyDrawError = 400, LuckyDrawShopError = 401, LuckyDrawPlayError = 402, LuckyDrawFindMoreError = 403, ApiKeyError = 900, SecretKeyError = 901, AppIdError = 902, DeviceTokenError = 903, AuthorizationError = 904, AdobeAnalyticsError = 905, ViewControllerError = 910, WidgetConfigError = 911
  public func getCode() -> Swift.Int
  public func getMessage() -> Swift.String
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@_inheritsConvenienceInitializers @objc public class LPWidgetProfile : ObjectiveC.NSObject {
  @objc public var firstName: Swift.String?
  @objc public var lastName: Swift.String?
  @objc public var email: Swift.String?
  @objc public var mobileDialingCode: Swift.String?
  @objc public var mobileNo: Swift.String?
  @objc public var memberId: Swift.String?
  @objc override dynamic public init()
  @objc deinit
}
public struct TwitterPagerTabStripSettings {
  public struct Style {
    public var dotColor: UIKit.UIColor
    public var selectedDotColor: UIKit.UIColor
    public var portraitTitleFont: UIKit.UIFont
    public var landscapeTitleFont: UIKit.UIFont
    public var titleColor: UIKit.UIColor
  }
  public var style: LiquidPayWidget.TwitterPagerTabStripSettings.Style
}
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) open class TwitterPagerTabStripViewController : LiquidPayWidget.PagerTabStripViewController, LiquidPayWidget.PagerTabStripDataSource, LiquidPayWidget.PagerTabStripIsProgressiveDelegate {
  @_Concurrency.MainActor(unsafe) open var settings: LiquidPayWidget.TwitterPagerTabStripSettings
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewDidLoad()
  @_Concurrency.MainActor(unsafe) override open func reloadPagerTabStripView()
  @_Concurrency.MainActor(unsafe) open func updateIndicator(for viewController: LiquidPayWidget.PagerTabStripViewController, fromIndex: Swift.Int, toIndex: Swift.Int, withProgressPercentage progressPercentage: CoreGraphics.CGFloat, indexWasChanged: Swift.Bool)
  @_Concurrency.MainActor(unsafe) open func updateIndicator(for viewController: LiquidPayWidget.PagerTabStripViewController, fromIndex: Swift.Int, toIndex: Swift.Int)
  @objc override dynamic open func observeValue(forKeyPath keyPath: Swift.String?, of object: Any?, change: [Foundation.NSKeyValueChangeKey : Any]?, context: Swift.UnsafeMutableRawPointer?)
  @objc deinit
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewDidLayoutSubviews()
}
@objc @_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @_Concurrency.MainActor(unsafe) open class BarView : UIKit.UIView {
  @_Concurrency.MainActor(unsafe) open var selectedBar: UIKit.UIView {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @_Concurrency.MainActor(unsafe) open func moveTo(index: Swift.Int, animated: Swift.Bool)
  @_Concurrency.MainActor(unsafe) open func move(fromIndex: Swift.Int, toIndex: Swift.Int, progressPercentage: CoreGraphics.CGFloat)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func layoutSubviews()
  @objc deinit
}
extension UIKit.UIScrollView {
  @_Concurrency.MainActor(unsafe) public var stickyHeader: LiquidPayWidget.StickyHeader! {
    get
  }
}
@objc @_Concurrency.MainActor(unsafe) open class ButtonBarViewCell : UIKit.UICollectionViewCell {
  @objc @IBOutlet @_Concurrency.MainActor(unsafe) open var imageView: UIKit.UIImageView!
  @objc @IBOutlet @_Concurrency.MainActor(unsafe) open var label: UIKit.UILabel!
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var isSelected: Swift.Bool {
    @_Concurrency.MainActor(unsafe) @objc get
    @_Concurrency.MainActor(unsafe) @objc set
  }
  @objc deinit
}
public enum PagerTabStripError : Swift.Error {
  case viewControllerOutOfBounds
  public static func == (a: LiquidPayWidget.PagerTabStripError, b: LiquidPayWidget.PagerTabStripError) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public protocol DecodingContainerTransformer {
  associatedtype DecodingInput
  associatedtype DecodingOutput
  func decode(input: Self.DecodingInput) throws -> Self.DecodingOutput
}
extension Swift.KeyedDecodingContainer {
  public func decode<Transformer>(key: Swift.KeyedDecodingContainer<K>.Key, transformer: Transformer) throws -> Transformer.DecodingOutput where Transformer : LiquidPayWidget.DecodingContainerTransformer, Transformer.DecodingInput : Swift.Decodable
  public func decodeIfPresent<Transformer>(key: Swift.KeyedDecodingContainer<K>.Key, transformer: Transformer) throws -> Transformer.DecodingOutput? where Transformer : LiquidPayWidget.DecodingContainerTransformer, Transformer.DecodingInput : Swift.Decodable
}
@objc @_inheritsConvenienceInitializers public class StickyHeader : ObjectiveC.NSObject {
  weak public var scrollView: UIKit.UIScrollView? {
    get
    set
  }
  public var view: UIKit.UIView? {
    get
    set
  }
  public var height: CoreGraphics.CGFloat {
    get
    set
  }
  public var minimumHeight: CoreGraphics.CGFloat {
    get
    set
  }
  @objc override dynamic public func observeValue(forKeyPath keyPath: Swift.String?, of object: Any?, change: [Foundation.NSKeyValueChangeKey : Any]?, context: Swift.UnsafeMutableRawPointer?)
  @objc override dynamic public init()
  @objc deinit
}
@_inheritsConvenienceInitializers @objc public class LPWidgetConfig : ObjectiveC.NSObject {
  @objc public var appId: Swift.String?
  @objc public var apiKey: Swift.String?
  @objc public var apiSecret: Swift.String?
  @objc public var deviceToken: Swift.String?
  @objc public var analyticsAppId: Swift.String?
  @objc override dynamic public init()
  @objc deinit
}
@objc public enum LPWidgetType : Swift.Int {
  case Home
  case Payment
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@_inheritsConvenienceInitializers @objc public class LPWidget : ObjectiveC.NSObject {
  @objc override dynamic public init()
  @objc public init(widgetType: LiquidPayWidget.LPWidgetType, widgetConfig: LiquidPayWidget.LPWidgetConfig?, widgetProfile: LiquidPayWidget.LPWidgetProfile?, delegate: Swift.AnyObject)
  @objc public func show()
  @objc deinit
}
public enum SwipeDirection {
  case left
  case right
  case none
  public static func == (a: LiquidPayWidget.SwipeDirection, b: LiquidPayWidget.SwipeDirection) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum UPCarouselFlowLayoutSpacingMode {
  case fixed(spacing: CoreGraphics.CGFloat)
  case overlap(visibleOffset: CoreGraphics.CGFloat)
}
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) open class UPCarouselFlowLayout : UIKit.UICollectionViewFlowLayout {
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) open var sideItemScale: CoreGraphics.CGFloat
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) open var sideItemAlpha: CoreGraphics.CGFloat
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) open var sideItemShift: CoreGraphics.CGFloat
  @_Concurrency.MainActor(unsafe) open var spacingMode: LiquidPayWidget.UPCarouselFlowLayoutSpacingMode
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func prepare()
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func shouldInvalidateLayout(forBoundsChange newBounds: CoreGraphics.CGRect) -> Swift.Bool
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func layoutAttributesForElements(in rect: CoreGraphics.CGRect) -> [UIKit.UICollectionViewLayoutAttributes]?
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func targetContentOffset(forProposedContentOffset proposedContentOffset: CoreGraphics.CGPoint, withScrollingVelocity velocity: CoreGraphics.CGPoint) -> CoreGraphics.CGPoint
  @objc override dynamic public init()
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
public struct BarPagerTabStripSettings {
  public struct Style {
    public var barBackgroundColor: UIKit.UIColor?
    public var selectedBarBackgroundColor: UIKit.UIColor?
    public var barHeight: CoreGraphics.CGFloat
  }
  public var style: LiquidPayWidget.BarPagerTabStripSettings.Style
}
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) open class BarPagerTabStripViewController : LiquidPayWidget.PagerTabStripViewController, LiquidPayWidget.PagerTabStripDataSource, LiquidPayWidget.PagerTabStripIsProgressiveDelegate {
  @_Concurrency.MainActor(unsafe) public var settings: LiquidPayWidget.BarPagerTabStripSettings
  @objc @IBOutlet @_Concurrency.MainActor(unsafe) weak public var barView: LiquidPayWidget.BarView!
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewDidLoad()
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewWillAppear(_ animated: Swift.Bool)
  @_Concurrency.MainActor(unsafe) override open func reloadPagerTabStripView()
  @_Concurrency.MainActor(unsafe) open func updateIndicator(for viewController: LiquidPayWidget.PagerTabStripViewController, fromIndex: Swift.Int, toIndex: Swift.Int, withProgressPercentage progressPercentage: CoreGraphics.CGFloat, indexWasChanged: Swift.Bool)
  @_Concurrency.MainActor(unsafe) open func updateIndicator(for viewController: LiquidPayWidget.PagerTabStripViewController, fromIndex: Swift.Int, toIndex: Swift.Int)
  @objc deinit
}
public enum PagerScroll {
  case no
  case yes
  case scrollOnlyIfOutOfScreen
  public static func == (a: LiquidPayWidget.PagerScroll, b: LiquidPayWidget.PagerScroll) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum SelectedBarAlignment {
  case left
  case center
  case right
  case progressive
  public static func == (a: LiquidPayWidget.SelectedBarAlignment, b: LiquidPayWidget.SelectedBarAlignment) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum SelectedBarVerticalAlignment {
  case top
  case middle
  case bottom
  public static func == (a: LiquidPayWidget.SelectedBarVerticalAlignment, b: LiquidPayWidget.SelectedBarVerticalAlignment) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) open class ButtonBarView : UIKit.UICollectionView {
  @_Concurrency.MainActor(unsafe) open var selectedBar: UIKit.UIView {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(frame: CoreGraphics.CGRect, collectionViewLayout layout: UIKit.UICollectionViewLayout)
  @_Concurrency.MainActor(unsafe) open func moveTo(index: Swift.Int, animated: Swift.Bool, swipeDirection: LiquidPayWidget.SwipeDirection, pagerScroll: LiquidPayWidget.PagerScroll)
  @_Concurrency.MainActor(unsafe) open func move(fromIndex: Swift.Int, toIndex: Swift.Int, progressPercentage: CoreGraphics.CGFloat, pagerScroll: LiquidPayWidget.PagerScroll)
  @_Concurrency.MainActor(unsafe) open func updateSelectedBarPosition(_ animated: Swift.Bool, swipeDirection: LiquidPayWidget.SwipeDirection, pagerScroll: LiquidPayWidget.PagerScroll)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func layoutSubviews()
  @objc deinit
}
public enum ButtonBarItemSpec<CellType> where CellType : UIKit.UICollectionViewCell {
  case nibFile(nibName: Swift.String, bundle: Foundation.Bundle?, width: ((LiquidPayWidget.IndicatorInfo) -> CoreGraphics.CGFloat))
  case cellClass(width: ((LiquidPayWidget.IndicatorInfo) -> CoreGraphics.CGFloat))
  public var weight: ((LiquidPayWidget.IndicatorInfo) -> CoreGraphics.CGFloat) {
    get
  }
}
public struct ButtonBarPagerTabStripSettings {
  public struct Style {
    public var buttonBarBackgroundColor: UIKit.UIColor?
    public var buttonBarMinimumInteritemSpacing: CoreGraphics.CGFloat?
    public var buttonBarMinimumLineSpacing: CoreGraphics.CGFloat?
    public var buttonBarLeftContentInset: CoreGraphics.CGFloat?
    public var buttonBarRightContentInset: CoreGraphics.CGFloat?
    public var selectedBarBackgroundColor: UIKit.UIColor
    public var selectedBarHeight: CoreGraphics.CGFloat
    public var selectedBarVerticalAlignment: LiquidPayWidget.SelectedBarVerticalAlignment
    public var buttonBarItemBackgroundColor: UIKit.UIColor?
    public var buttonBarItemFont: UIKit.UIFont
    public var buttonBarItemLeftRightMargin: CoreGraphics.CGFloat
    public var buttonBarItemTitleColor: UIKit.UIColor?
    public var buttonBarItemsShouldFillAvailiableWidth: Swift.Bool {
      get
      set
    }
    public var buttonBarItemsShouldFillAvailableWidth: Swift.Bool
    public var buttonBarHeight: CoreGraphics.CGFloat?
  }
  public var style: LiquidPayWidget.ButtonBarPagerTabStripSettings.Style
}
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) open class ButtonBarPagerTabStripViewController : LiquidPayWidget.PagerTabStripViewController, LiquidPayWidget.PagerTabStripDataSource, LiquidPayWidget.PagerTabStripIsProgressiveDelegate, UIKit.UICollectionViewDelegate, UIKit.UICollectionViewDataSource {
  @_Concurrency.MainActor(unsafe) public var settings: LiquidPayWidget.ButtonBarPagerTabStripSettings
  @_Concurrency.MainActor(unsafe) public var buttonBarItemSpec: LiquidPayWidget.ButtonBarItemSpec<LiquidPayWidget.ButtonBarViewCell>!
  @_Concurrency.MainActor(unsafe) public var changeCurrentIndex: ((_ oldCell: LiquidPayWidget.ButtonBarViewCell?, _ newCell: LiquidPayWidget.ButtonBarViewCell?, _ animated: Swift.Bool) -> Swift.Void)?
  @_Concurrency.MainActor(unsafe) public var changeCurrentIndexProgressive: ((_ oldCell: LiquidPayWidget.ButtonBarViewCell?, _ newCell: LiquidPayWidget.ButtonBarViewCell?, _ progressPercentage: CoreGraphics.CGFloat, _ changeCurrentIndex: Swift.Bool, _ animated: Swift.Bool) -> Swift.Void)?
  @objc @IBOutlet @_Concurrency.MainActor(unsafe) weak public var buttonBarView: LiquidPayWidget.ButtonBarView!
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewDidLoad()
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewWillAppear(_ animated: Swift.Bool)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewDidLayoutSubviews()
  @_Concurrency.MainActor(unsafe) override open func reloadPagerTabStripView()
  @_Concurrency.MainActor(unsafe) open func calculateStretchedCellWidths(_ minimumCellWidths: [CoreGraphics.CGFloat], suggestedStretchedCellWidth: CoreGraphics.CGFloat, previousNumberOfLargeCells: Swift.Int) -> CoreGraphics.CGFloat
  @_Concurrency.MainActor(unsafe) open func updateIndicator(for viewController: LiquidPayWidget.PagerTabStripViewController, fromIndex: Swift.Int, toIndex: Swift.Int)
  @_Concurrency.MainActor(unsafe) open func updateIndicator(for viewController: LiquidPayWidget.PagerTabStripViewController, fromIndex: Swift.Int, toIndex: Swift.Int, withProgressPercentage progressPercentage: CoreGraphics.CGFloat, indexWasChanged: Swift.Bool)
  @objc @_Concurrency.MainActor(unsafe) open func collectionView(_ collectionView: UIKit.UICollectionView, layout collectionViewLayout: UIKit.UICollectionViewLayout, sizeForItemAtIndexPath indexPath: Foundation.IndexPath) -> CoreGraphics.CGSize
  @_Concurrency.MainActor(unsafe) @objc open func collectionView(_ collectionView: UIKit.UICollectionView, didSelectItemAt indexPath: Foundation.IndexPath)
  @_Concurrency.MainActor(unsafe) @objc open func collectionView(_ collectionView: UIKit.UICollectionView, numberOfItemsInSection section: Swift.Int) -> Swift.Int
  @_Concurrency.MainActor(unsafe) @objc open func collectionView(_ collectionView: UIKit.UICollectionView, cellForItemAt indexPath: Foundation.IndexPath) -> UIKit.UICollectionViewCell
  @_Concurrency.MainActor(unsafe) @objc override open func scrollViewDidEndScrollingAnimation(_ scrollView: UIKit.UIScrollView)
  @_Concurrency.MainActor(unsafe) open func configureCell(_ cell: LiquidPayWidget.ButtonBarViewCell, indicatorInfo: LiquidPayWidget.IndicatorInfo)
  @objc deinit
}
public struct IndicatorInfo {
  public var title: Swift.String?
  public var image: UIKit.UIImage?
  public var highlightedImage: UIKit.UIImage?
  public var accessibilityLabel: Swift.String?
  public var userInfo: Any?
  public init(title: Swift.String?)
  public init(image: UIKit.UIImage?, highlightedImage: UIKit.UIImage? = nil, userInfo: Any? = nil)
  public init(title: Swift.String?, image: UIKit.UIImage?, highlightedImage: UIKit.UIImage? = nil, userInfo: Any? = nil)
  public init(title: Swift.String?, accessibilityLabel: Swift.String?, image: UIKit.UIImage?, highlightedImage: UIKit.UIImage? = nil, userInfo: Any? = nil)
}
extension LiquidPayWidget.IndicatorInfo : Swift.ExpressibleByStringLiteral {
  public init(stringLiteral value: Swift.String)
  public init(extendedGraphemeClusterLiteral value: Swift.String)
  public init(unicodeScalarLiteral value: Swift.String)
  public typealias ExtendedGraphemeClusterLiteralType = Swift.String
  public typealias StringLiteralType = Swift.String
  public typealias UnicodeScalarLiteralType = Swift.String
}
@objc public protocol LPWidgetDelegate {
  @objc func widgetDidClose()
  @objc func widgetOnError(widgetStatus: LiquidPayWidget.LPWidgetStatus)
}
public struct SegmentedPagerTabStripSettings {
  public struct Style {
    public var segmentedControlColor: UIKit.UIColor?
  }
  public var style: LiquidPayWidget.SegmentedPagerTabStripSettings.Style
}
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) open class SegmentedPagerTabStripViewController : LiquidPayWidget.PagerTabStripViewController, LiquidPayWidget.PagerTabStripDataSource, LiquidPayWidget.PagerTabStripDelegate {
  @objc @IBOutlet @_Concurrency.MainActor(unsafe) weak public var segmentedControl: UIKit.UISegmentedControl!
  @_Concurrency.MainActor(unsafe) open var settings: LiquidPayWidget.SegmentedPagerTabStripSettings
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewDidLoad()
  @_Concurrency.MainActor(unsafe) override open func reloadPagerTabStripView()
  @_Concurrency.MainActor(unsafe) open func updateIndicator(for viewController: LiquidPayWidget.PagerTabStripViewController, fromIndex: Swift.Int, toIndex: Swift.Int)
  @_Concurrency.MainActor(unsafe) @objc override open func scrollViewDidEndScrollingAnimation(_ scrollView: UIKit.UIScrollView)
  @objc deinit
}
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) public class LPGradient : UIKit.UIView {
  @_Concurrency.MainActor(unsafe) @objc override dynamic public class var layerClass: Swift.AnyClass {
    @_Concurrency.MainActor(unsafe) @objc get
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic public func traitCollectionDidChange(_ previousTraitCollection: UIKit.UITraitCollection?)
  @_Concurrency.MainActor(unsafe) @objc override dynamic public func willMove(toSuperview newSuperview: UIKit.UIView?)
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
@_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) open class BaseButtonBarPagerTabStripViewController<ButtonBarCellType> : LiquidPayWidget.PagerTabStripViewController, LiquidPayWidget.PagerTabStripDataSource, LiquidPayWidget.PagerTabStripIsProgressiveDelegate, UIKit.UICollectionViewDelegate, UIKit.UICollectionViewDataSource where ButtonBarCellType : UIKit.UICollectionViewCell {
  @_Concurrency.MainActor(unsafe) public var settings: LiquidPayWidget.ButtonBarPagerTabStripSettings
  @_Concurrency.MainActor(unsafe) public var buttonBarItemSpec: LiquidPayWidget.ButtonBarItemSpec<ButtonBarCellType>!
  @_Concurrency.MainActor(unsafe) public var changeCurrentIndex: ((_ oldCell: ButtonBarCellType?, _ newCell: ButtonBarCellType?, _ animated: Swift.Bool) -> Swift.Void)?
  @_Concurrency.MainActor(unsafe) public var changeCurrentIndexProgressive: ((_ oldCell: ButtonBarCellType?, _ newCell: ButtonBarCellType?, _ progressPercentage: CoreGraphics.CGFloat, _ changeCurrentIndex: Swift.Bool, _ animated: Swift.Bool) -> Swift.Void)?
  @objc @IBOutlet @_Concurrency.MainActor(unsafe) weak public var buttonBarView: LiquidPayWidget.ButtonBarView!
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewDidLoad()
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewWillAppear(_ animated: Swift.Bool)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewDidAppear(_ animated: Swift.Bool)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewDidLayoutSubviews()
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewWillTransition(to size: CoreGraphics.CGSize, with coordinator: UIKit.UIViewControllerTransitionCoordinator)
  @_Concurrency.MainActor(unsafe) override open func reloadPagerTabStripView()
  @_Concurrency.MainActor(unsafe) open func calculateStretchedCellWidths(_ minimumCellWidths: [CoreGraphics.CGFloat], suggestedStretchedCellWidth: CoreGraphics.CGFloat, previousNumberOfLargeCells: Swift.Int) -> CoreGraphics.CGFloat
  @_Concurrency.MainActor(unsafe) open func updateIndicator(for viewController: LiquidPayWidget.PagerTabStripViewController, fromIndex: Swift.Int, toIndex: Swift.Int)
  @_Concurrency.MainActor(unsafe) open func updateIndicator(for viewController: LiquidPayWidget.PagerTabStripViewController, fromIndex: Swift.Int, toIndex: Swift.Int, withProgressPercentage progressPercentage: CoreGraphics.CGFloat, indexWasChanged: Swift.Bool)
  @objc @_Concurrency.MainActor(unsafe) open func collectionView(_ collectionView: UIKit.UICollectionView, layout collectionViewLayout: UIKit.UICollectionViewLayout, sizeForItemAtIndexPath indexPath: Foundation.IndexPath) -> CoreGraphics.CGSize
  @_Concurrency.MainActor(unsafe) @objc open func collectionView(_ collectionView: UIKit.UICollectionView, didSelectItemAt indexPath: Foundation.IndexPath)
  @_Concurrency.MainActor(unsafe) @objc open func collectionView(_ collectionView: UIKit.UICollectionView, numberOfItemsInSection section: Swift.Int) -> Swift.Int
  @_Concurrency.MainActor(unsafe) @objc open func collectionView(_ collectionView: UIKit.UICollectionView, cellForItemAt indexPath: Foundation.IndexPath) -> UIKit.UICollectionViewCell
  @_Concurrency.MainActor(unsafe) @objc override open func scrollViewDidEndScrollingAnimation(_ scrollView: UIKit.UIScrollView)
  @_Concurrency.MainActor(unsafe) open func configure(cell: ButtonBarCellType, for indicatorInfo: LiquidPayWidget.IndicatorInfo)
  @objc deinit
}
@_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) open class ExampleBaseButtonBarPagerTabStripViewController : LiquidPayWidget.BaseButtonBarPagerTabStripViewController<LiquidPayWidget.ButtonBarViewCell> {
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @_Concurrency.MainActor(unsafe) open func initialize()
  @_Concurrency.MainActor(unsafe) override open func configure(cell: LiquidPayWidget.ButtonBarViewCell, for indicatorInfo: LiquidPayWidget.IndicatorInfo)
  @objc deinit
}
extension LiquidPayWidget.LPWidgetStatus : Swift.Equatable {}
extension LiquidPayWidget.LPWidgetStatus : Swift.Hashable {}
extension LiquidPayWidget.LPWidgetStatus : Swift.RawRepresentable {}
extension LiquidPayWidget.PagerTabStripError : Swift.Equatable {}
extension LiquidPayWidget.PagerTabStripError : Swift.Hashable {}
extension LiquidPayWidget.LPWidgetType : Swift.Equatable {}
extension LiquidPayWidget.LPWidgetType : Swift.Hashable {}
extension LiquidPayWidget.LPWidgetType : Swift.RawRepresentable {}
extension LiquidPayWidget.SwipeDirection : Swift.Equatable {}
extension LiquidPayWidget.SwipeDirection : Swift.Hashable {}
extension LiquidPayWidget.PagerScroll : Swift.Equatable {}
extension LiquidPayWidget.PagerScroll : Swift.Hashable {}
extension LiquidPayWidget.SelectedBarAlignment : Swift.Equatable {}
extension LiquidPayWidget.SelectedBarAlignment : Swift.Hashable {}
extension LiquidPayWidget.SelectedBarVerticalAlignment : Swift.Equatable {}
extension LiquidPayWidget.SelectedBarVerticalAlignment : Swift.Hashable {}
