Pod::Spec.new do |s|
s.name              = 'LiquidPayWidget'
s.version           = '1.2.2'
s.summary           = 'Use the Liquid Widget SDK...'
s.homepage          = 'https://liquidpaygroup.com'

s.author            = { 'Liquid Group Pte' => 'info@liquidpay.com' }
s.license           = { :type => 'Apache-2.0', :file => 'LICENSE' }

s.platform          = :ios
s.source            = { :git => 'https://gitlab.com/liquidgroup-opensource/sdks/liquid.ios.sdk', :tag => '1.2.2' }

s.dependency 'ACPCore', '~> 2.0'
s.dependency 'ACPAnalytics', '~> 2.0'
s.dependency 'ACPAudience', '~> 2.0'
s.dependency 'ACPTarget', '~> 2.0'
s.dependency 'AEPAssurance', '~> 1.0'
s.dependency 'ACPUserProfile', '~> 2.0'

s.pod_target_xcconfig = {
    'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
  }
s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

s.ios.deployment_target = '10.0'
s.ios.vendored_frameworks = 'LiquidPayWidget.xcframework'
end
